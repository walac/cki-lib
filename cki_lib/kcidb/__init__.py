"""This module provides helpers for CKI's implementation of the KCIDB schema."""

from .file import KCIDBFile
from .file import ObjectNotFound
from .validate import KCIDB_SCHEMA
from .validate import KCIDB_STATUS_CHOICES
from .validate import SCHEMA as CKI_SCHEMA
from .validate import ValidationError
from .validate import sanitize_all_kcidb_status
from .validate import sanitize_kcidb_status
from .validate import validate_extended_kcidb_schema

MAJOR_VERSION = KCIDB_SCHEMA.major
MINOR_VERSION = KCIDB_SCHEMA.minor
