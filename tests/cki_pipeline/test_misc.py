"""Tests for various methods of cki_pipeline."""
# pylint: disable=protected-access
import unittest
from unittest.mock import patch

import responses
import yaml

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestMisc(unittest.TestCase, mocks.GitLabMocks):
    """Test cases for various functions in the cki_pipeline module."""

    def setUp(self):
        self.add_project(1, 'cki-project')
        self.add_branch(1, 'branch')
        self.add_gitlab_yml(1, {'include': [{
            'project': 'cki-project/pipeline-definition',
            'file': 'cki_pipeline.yml'
        }]})
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines?'
            'ref=branch&per_page=100&scope=finished',
            json=[{'id': 103}, {'id': 102}, {'id': 101}, {'id': 100}, {'id': 99}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines?'
            'ref=branch&per_page=100&scope=finished&status=success',
            json=[{'id': 103}, {'id': 102}, {'id': 100}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/99/variables',
            json=[{'key': 'git_url', 'value': 'foo'}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/100/variables',
            json=[{'key': 'git_url', 'value': 'different'}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/101/variables',
            json=[{'key': 'git_url', 'value': 'git'}])
        responses.add(
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/102/variables',
            json=[{'key': 'git_url', 'value': 'git'}])
        responses.add(  # retriggered
            responses.GET,
            'https://gitlab.com/api/v4/projects/1/pipelines/103/variables',
            json=[{'key': 'CKI_DEPLOYMENT_ENVIRONMENT', 'value': 'retrigger'},
                  {'key': 'git_url', 'value': 'git'}])

    def test__commit_message(self):
        """Check _commit_message."""
        variables = {
            'zoolander': 1,
            'foo': 2,
            'bar': 3,
            'title': 'test _commit_message',
            'qux': 4
        }
        expected = ("test _commit_message\n\nbar = 3\nfoo = 2\n"
                    "qux = 4\ntitle = test _commit_message\nzoolander = 1")

        self.assertEqual(cki_pipeline._commit_message(variables), expected)

    @responses.activate
    def test_last_pipeline(self):
        """Check the last pipeline."""
        project = get_instance('https://gitlab.com').projects.get('cki-project')
        cases = (
            ('no filter', {}, 102),
            ('filter', {'variable_filter': {'git_url': 'different'}}, 100),
            ('maxcount=1', {'variable_filter': {'git_url': 'different'}, 'max_count': 1}, None),
            ('maxcount=10', {'variable_filter': {'git_url': 'different'}, 'max_count': 10}, 100),
            ('maxcount=100', {'variable_filter': {'git_url': 'different'}, 'max_count': 100}, 100),
        )
        for description, args, expected in cases:
            with self.subTest(description):
                gl_pipeline = cki_pipeline.last_pipeline_for_branch(
                    project, 'branch', list_filter={"scope": 'finished'}, **args)
                if expected is None:
                    self.assertIsNone(gl_pipeline)
                else:
                    self.assertEqual(gl_pipeline.id, expected)

    @responses.activate
    def test_last_pipelines(self):
        """Check the last pipelines."""
        project = get_instance('https://gitlab.com').projects.get('cki-project')
        cases = (
            ('pipeline count 1', {}, [102]),
            ('pipeline count 2', {'pipeline_count': 2}, [102, 101]),
            ('pipeline count 10', {'pipeline_count': 10}, [102, 101, 100, 99]),
        )
        for description, args, expected in cases:
            with self.subTest(description):
                gl_pipelines = cki_pipeline.last_pipelines_for_branch(
                    project, 'branch', list_filter={"scope": 'finished'}, **args)
                self.assertEqual([p.id for p in gl_pipelines], expected)

    @responses.activate
    def test_last_successful_pipeline(self):
        """Check the last successful pipeline."""
        project = get_instance('https://gitlab.com').projects.get('cki-project')
        cases = (
            ('no filter', {}, 102),
            ('filter', {'variable_filter': {'git_url': 'different'}}, 100),
            ('filter list', {'variable_filter': {'git_url': [None, 'different']}}, 100),
            ('missing', {'variable_filter': {'git_url': 'non-existing'}}, None),
            ('none', {'variable_filter': {'variable': None}}, 102),
            ('none list', {'variable_filter': {'variable': [None, 'non-existing']}}, 102),
        )
        for description, args, expected in cases:
            with self.subTest(description):
                gl_pipeline = cki_pipeline.last_successful_pipeline_for_branch(
                    project, 'branch', **args)
                if expected is None:
                    self.assertIsNone(gl_pipeline)
                else:
                    self.assertEqual(gl_pipeline.id, expected)

    def test_clean_project_url(self):
        """Verify the conversion of repository URLs into GitLab project URLs."""

        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test.git/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test.git"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("https://foo.bar/test/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("foo.bar/test.git/"),
                         "https://foo.bar/test")
        self.assertEqual(cki_pipeline._clean_project_url("git+https://foo.bar/test.git/"),
                         "https://foo.bar/test")

    @responses.activate
    def test_create_custom_configuration(self):
        """Test for when .gitlab-ci.yml contains url includes"""
        variables = {
            'cki_pipeline_branch': 'cki_pipeline_branch',
            'title': 'title',
            'pipeline_definition_branch_override': 'pipeline-branch',
        }
        expected = yaml.dump({'include': [{
            'project': 'cki-project/pipeline-definition',
            'file': 'cki_pipeline.yml',
            'ref': variables['pipeline_definition_branch_override']
        }]})

        project = get_instance('https://gitlab.com').projects.get('cki-project')
        start_sha = 'sha'
        with patch.object(project.commits, "create") as mock_create:
            branch = cki_pipeline._create_custom_configuration(project, start_sha, variables)
            mock_create.assert_called_with({
                'branch': branch,
                'start_sha': start_sha,
                'commit_message': cki_pipeline._commit_message(variables),
                'actions': [{
                    'action': 'update',
                    'file_path': '.gitlab-ci.yml',
                    'content': expected
                }]
            })
