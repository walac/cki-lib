---
title: cki_entrypoint.sh
description: >-
    Best-practice container entrypoint script for CKI container images
---

The entrypoint script provides a best-practice container entrypoint script for
CKI (Python) container images. It is configured via environment variables:

| Variable            | Description                                                                                    |
|---------------------|------------------------------------------------------------------------------------------------|
| `START_REDIS`       | Start a Redis server                                                                           |
| `START_PHP`         | Start PHP FastCGI process manager                                                              |
| `START_APACHE`      | Start Apache web server                                                                        |
| `START_CELERY_xxxx` | Start Celery workers with the given command line arguments, one worker per variable            |
| `START_FLASK`       | Start the given Flask app via gunicorn (production) or the Flask devel server                  |
| `START_PYTHON_xxxx` | Start Python modules with the given command line arguments, one module per variable            |
| `LOG_NAME`          | Append all stdout/stderr output to `/logs/$LOG_NAME-$HOSTNAME.log`                             |
| `LOG_USE_HOSTNAME`  | If false, do not include the host name in the log file name, i.e. log to `/logs/$LOG_NAME.log` |
| `KRB_REFRESH`       | If true, start a `krb_ticket_refresher` process in the background                              |

These variables should be set in the Containerfile, e.g.

The following example starts a Redis server, two celery workers and a Flask app:

```docker
#!/bin/bash
ENV START_REDIS=true
ENV START_CELERY_MATCHER="--app pipeline_herder.worker --concurrency 1 --queues matching"
ENV START_CELERY_HERDER="--app pipeline_herder.worker --concurrency 1 --queues herding"
ENV START_FLASK=pipeline_herder.webhook
ENV LOG_NAME=pipeline-herder

# CMD ["cki_entrypoint.sh"]  # mostly not needed as already set for CKI Python image
```

In case a separate entrypoint script is needed, the chaining must happen via
exec like

```bash
exec cki_entrypoint.sh
```

Otherwise, the process tree cannot be killed on failures!
