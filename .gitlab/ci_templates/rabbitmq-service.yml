---
.rabbitmq_service:
  services:
    # docker.io/rabbitmq:3-management-alpine
    - name: quay.io/cki/mirror_rabbitmq:3-management-alpine
      alias: rabbitmq
      entrypoint: [/bin/sh]
      command:
        - -c
        - |
          mkdir -p "${RABBITMQ_SERVICE_DIR}"
          (
          mkdir -p "${RABBITMQ_SERVICE_DIR}/certs" "${RABBITMQ_SERVICE_DIR}/private"
          chmod 700 "${RABBITMQ_SERVICE_DIR}/private"
          echo 01 > "${RABBITMQ_SERVICE_DIR}/serial"
          touch "${RABBITMQ_SERVICE_DIR}/index.txt"
          cat << EOF > "${RABBITMQ_SERVICE_DIR}/openssl.cnf"
          [ ca ]
          default_ca = rabbitmqca

          [ rabbitmqca ]
          certificate = ${RABBITMQ_SERVICE_DIR}/ca_certificate.pem
          database = ${RABBITMQ_SERVICE_DIR}/index.txt
          new_certs_dir = ${RABBITMQ_SERVICE_DIR}/certs
          private_key = ${RABBITMQ_SERVICE_DIR}/ca_private_key.pem
          serial = ${RABBITMQ_SERVICE_DIR}/serial
          copy_extensions = copy

          default_crl_days = 7
          default_days = 365000
          default_md = sha256

          policy = rabbitmqca_policy
          x509_extensions = certificate_extensions

          [ rabbitmqca_policy ]
          commonName = supplied
          stateOrProvinceName = optional
          countryName = optional
          emailAddress = optional
          organizationName = optional
          organizationalUnitName = optional
          domainComponent = optional

          [ certificate_extensions ]
          basicConstraints = CA:false

          [ req ]
          default_bits = 2048
          default_keyfile = ${RABBITMQ_SERVICE_DIR}/ca_private_key.pem
          default_md = sha256
          prompt = yes
          distinguished_name = root_ca_distinguished_name
          x509_extensions = root_ca_extensions

          [ root_ca_distinguished_name ]
          commonName = hostname

          [ root_ca_extensions ]
          basicConstraints = CA:true
          keyUsage = keyCertSign, cRLSign

          [ client_ca_extensions ]
          basicConstraints = CA:false
          keyUsage = digitalSignature,keyEncipherment
          extendedKeyUsage = 1.3.6.1.5.5.7.3.2

          [ server_ca_extensions ]
          basicConstraints = CA:false
          keyUsage = digitalSignature,keyEncipherment
          extendedKeyUsage = serverAuth
          EOF
          openssl req \
              -config ${RABBITMQ_SERVICE_DIR}/openssl.cnf \
              -x509 \
              -newkey rsa:2048 \
              -days 365000 \
              -subj '/CN=RabbitMQ CA/' \
              -nodes \
              -outform PEM \
              -out "${RABBITMQ_SERVICE_DIR}/ca_certificate.pem" \
              -keyout "${RABBITMQ_SERVICE_DIR}/ca_private_key.pem"
          openssl genrsa -out "${RABBITMQ_SERVICE_DIR}/server_private_key.pem"
          openssl req \
              -new \
              -subj "/CN=host/" \
              -nodes \
              -outform PEM \
              -addext "subjectAltName = DNS:rabbitmq,DNS:localhost" \
              -key "${RABBITMQ_SERVICE_DIR}/server_private_key.pem" \
              -out "${RABBITMQ_SERVICE_DIR}/server_req.pem"
          openssl ca \
              -notext \
              -batch \
              -extensions server_ca_extensions \
              -config "${RABBITMQ_SERVICE_DIR}/openssl.cnf" \
              -in "${RABBITMQ_SERVICE_DIR}/server_req.pem" \
              -out "${RABBITMQ_SERVICE_DIR}/server_certificate.pem"
          openssl genrsa -out "${RABBITMQ_SERVICE_DIR}/client_private_key.pem"
          openssl req \
              -new \
              -subj /CN=guest/O=client/ \
              -nodes \
              -outform PEM \
              -key "${RABBITMQ_SERVICE_DIR}/client_private_key.pem" \
              -out "${RABBITMQ_SERVICE_DIR}/client_req.pem"
          openssl ca \
              -notext \
              -batch \
              -extensions client_ca_extensions \
              -config "${RABBITMQ_SERVICE_DIR}/openssl.cnf" \
              -in "${RABBITMQ_SERVICE_DIR}/client_req.pem" \
              -out "${RABBITMQ_SERVICE_DIR}/client_certificate.pem"
          cat "${RABBITMQ_SERVICE_DIR}/client_private_key.pem" \
              "${RABBITMQ_SERVICE_DIR}/client_certificate.pem" > \
              "${RABBITMQ_SERVICE_DIR}/client_private_key_and_certificate.pem"
          rabbitmq-plugins enable --offline rabbitmq_stomp
          cat << EOF > /etc/rabbitmq/conf.d/ssl.conf
          listeners.ssl.default  = 5671
          ssl_options.cacertfile = ${RABBITMQ_SERVICE_DIR}/ca_certificate.pem
          ssl_options.certfile   = ${RABBITMQ_SERVICE_DIR}/server_certificate.pem
          ssl_options.keyfile    = ${RABBITMQ_SERVICE_DIR}/server_private_key.pem
          ssl_options.versions.1 = tlsv1.2
          ssl_options.verify     = verify_peer
          ssl_options.fail_if_no_peer_cert = true
          ssl_cert_login_from    = common_name
          stomp.listeners.ssl.1  = 61614
          stomp.ssl_cert_login   = true
          EOF
          rabbitmq-server
          ) > "${RABBITMQ_SERVICE_DIR}/output.txt"
  variables:
    RABBITMQ_HOST: 'rabbitmq'
    RABBITMQ_SERVICE_DIR: "/builds/rabbitmq-${CI_PIPELINE_ID}"
    RABBITMQ_CLIENT_PRIVATE_KEY_AND_CERTIFICATE: "${RABBITMQ_SERVICE_DIR}/client_private_key_and_certificate.pem"
    STOMP_HOST: 'rabbitmq'
    STOMP_PORT: '61614'
    STOMP_CERTFILE: "${RABBITMQ_SERVICE_DIR}/client_private_key_and_certificate.pem"
  before_script:
    - tail -f -n +1 "${RABBITMQ_SERVICE_DIR}/output.txt" &
